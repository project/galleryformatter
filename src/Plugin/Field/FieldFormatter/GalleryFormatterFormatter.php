<?php

namespace Drupal\galleryformatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Utility\LinkGeneratorInterface;
use Drupal\responsive_image\Plugin\Field\FieldFormatter\ResponsiveImageFormatter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin for galleryformatter.
 *
 * @FieldFormatter(
 *   id = "galleryformatter",
 *   label = @Translation("jQuery Gallery"),
 *   field_types = {
 *     "image",
 *   }
 * )
 */
class GalleryFormatterFormatter extends ResponsiveImageFormatter {

  /**
   * Returns the module_handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityStorageInterface $responsive_image_style_storage, EntityStorageInterface $image_style_storage, LinkGeneratorInterface $link_generator, AccountInterface $current_user, ModuleHandlerInterface $module_handler) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $responsive_image_style_storage, $image_style_storage, $link_generator, $current_user);
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')->getStorage('responsive_image_style'),
      $container->get('entity_type.manager')->getStorage('image_style'),
      $container->get('link_generator'),
      $container->get('current_user'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'slide_style' => 'wide',
      'thumb_style' => 'narrow',
      'style' => 'galleryformatter',
      'modal' => '',
      'previous' => '<',
      'next' => '>',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    // Get a list of all style names for our elements options.
    $options = [];
    foreach ($this->responsiveImageStyleStorage->loadMultiple() as $id => $style) {
      $options[$id] = $id;
    }

    $elements['slide_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Select the slide style'),
      '#options' => $options,
      '#default_value' => $this->getSetting('slide_style'),
      '#description' => $this->t('Select the imagecache style you would like to show when clicked on the thumbnail.'),
    ];
    $elements['thumb_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Select the thumbnail style'),
      '#options' => $options,
      '#default_value' => $this->getSetting('thumb_style'),
      '#description' => $this->t('Select the imagecache style you would like to show for the thumbnails list.'),
    ];

    $styles = (array) $this->moduleHandler->invokeAll('galleryformatter_styles');
    // The keys used for options must be valid html id-s.
    $style_options = [];
    foreach ($styles as $key => $style) {
      if (isset($style['label'])) {
        $style_options[$key] = $style['label'];
      }
    }
    ksort($style_options);
    $elements['style'] = [
      '#type' => 'select',
      '#title' => $this->t('Style'),
      '#options' => ['nostyle' => $this->t('No style')] + $style_options,
      '#default_value' => $this->getSetting('style'),
      '#description' => $this->t('Choose the gallery style.'),
    ];
    $modal_options = [];
    // Integration with other modules for jQuery modal windows.
    if ($this->moduleHandler->moduleExists('colorbox')) {
      $modal_options['colorbox'] = 'colorbox';
    }
    if ($this->moduleHandler->moduleExists('shadowbox')) {
      $modal_options['shadowbox'] = 'shadowbox';
    }
    if ($this->moduleHandler->moduleExists('lightbox2')) {
      $modal_options['lightbox2'] = 'lightbox2';
    }
    if ($this->moduleHandler->moduleExists('fancybox')) {
      $modal_options['fancybox'] = 'fancybox';
    }
    $modal_options['none'] = $this->t('Do not use modal');
    $elements['modal'] = [
      '#type' => 'select',
      '#title' => $this->t('Use jQuery modal for full image link'),
      '#options' => $modal_options,
      '#default_value' => $this->getSetting('modal'),
      '#description' => $this->t("Select which jQuery modal module you'd like to display the full link image in, if any."),
    ];

    $elements['previous'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Previous button'),
      '#default_value' => $this->getSetting('previous'),
    ];
    $elements['next'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Next button'),
      '#default_value' => $this->getSetting('next'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $settings = $this->getSettings();
    $summary = parent::settingsSummary();

    if (!empty($settings['slide_style']) || !empty($settings['thumb_style'])) {
      $summary[] = $this->t('Slides style: @value', ['@value' => $settings['slide_style']]);
      $summary[] = $this->t('Thumbnails style: @value', ['@value' => $settings['thumb_style']]);
      $summary[] = $this->t('Gallery style: @value', ['@value' => $settings['style']]);
      $summary[] = $this->t('Modal: @value', ['@value' => $settings['modal']]);
    }
    else {
      $summary[] = $this->t('Customize your options for the jQuery Gallery.');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    $settings = $this->getSettings();

    foreach ($elements as $key => $item) {
      if ($this->getSetting('slide_style') && $style = $this->responsiveImageStyleStorage->load($this->getSetting('slide_style'))) {
        $elements[$key]['#responsive_image_style_id'] = $style->id();
      }
    }
    // Duplicate each element and set it a thumb_style image style.
    $thumb_elements = [];
    foreach ($elements as $element) {
      $new_element = $element;
      if ($this->getSetting('thumb_style') && $style = $this->responsiveImageStyleStorage->load($this->getSetting('thumb_style'))) {
        $new_element['#responsive_image_style_id'] = $style->id();
      }
      $thumb_elements[] = $new_element;
    }

    $extra = [];
    if ('nostyle' != $settings['style']) {
      $styles = (array) $this->moduleHandler->invokeAll('galleryformatter_styles');
      if (!empty($styles[$settings['style']]['library'])) {
        $extra['library'] = [$styles[$settings['style']]['library']];
      }
    }

    return [
      '#theme' => 'galleryformatter',
      '#slides' => $elements,
      '#thumbs' => $thumb_elements,
      '#settings' => $settings,
      '#dimensions' => '',
      '#attached' => [
        'drupalSettings' => [
          'galleryformatter' => [
            'previous' => $this->getSetting('previous'),
            'next' => $this->getSetting('next'),
          ],
        ],
      ] + $extra,
    ];
  }

}
