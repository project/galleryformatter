<?php

/**
 * @file
 * Hooks for the galleryformatter module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Provides custom gallery styles.
 */
function hook_galleryformatter_styles() {
  return [
    'inline-gallery' => [
      'label' => t('Inline gallery'),
      'library' => 'galleryformatter/galleryformatter',
    ],
  ];
}

/**
 * @} End of "addtogroup hooks".
 */
