# Gallery formatter

Gallery formatter provides a CCK formatter for image fields,
which will turn any image field into a jQuery Gallery.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/galleryformatter).

To submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/galleryformatter).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Select the 'jQuery Gallery' formatter in the display section for your image field.


## Maintainers

- Manuel Garcia - [manuel garcia](https://www.drupal.org/u/manuel-garcia)
- Viktor Holovachek - [AstonVictor](https://www.drupal.org/u/astonvictor)
